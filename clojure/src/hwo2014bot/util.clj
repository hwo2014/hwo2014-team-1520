(ns hwo2014bot.util
  (:require [clojure.data.json :as json])
  (:use [cheshire.core :only [generate-string]]
        [environ.core :only [env]]
        clojure.core.matrix.stats))

(def ping {:msgType "ping" :data "ping"})

(defn json->clj [string]
  (json/read-str string :key-fn keyword))

(defmacro dbg [body]
  `(let [x# ~body]
     (println "dbg:" '~body "=" x#)
     x#))

(def dbgout (env :dbgout))

(defn log-to-file [msg n & prefixes]
  (when dbgout
    (let [msg-string (generate-string msg {:pretty true})
          prefixes   (filter #(not (nil? %)) (flatten [(format "%06d" n) (:gameTick msg) prefixes (:msgType msg)]))
          name       (clojure.string/join "_" prefixes)
          filename   (format "./msglog/%s.json" name)]
      (clojure.java.io/make-parents filename)
      (spit filename msg-string))))


