(ns hwo2014bot.handlers
  (:use hwo2014bot.state
        hwo2014bot.util
        hwo2014bot.track
        hwo2014bot.car))

(defmulti handle-msg :msgType)

(defmethod handle-msg "joinRace" [_]
  ping)

(defmethod handle-msg "yourCar" [msg]
  (store :my-car (msg :data))
  ping)

(defmethod handle-msg "gameInit" [msg]
  (store :race (get-in msg [:data :race]))
  (future (analyze-track))
  #_(analyze-track)
  ping)

(defmethod handle-msg "carPositions" [msg]
  (cond
    (contains? msg :gameTick)
      (if (out-of-track?)
        {:msgType "throttle" :data 1.0}
        (throttle-response msg))
    :else
      (do
        (store :turbo nil)
        (store :turbo-on nil)
        (store :out-of-track false)
        (store :overtake-lane nil)
        (store :overtake-opponents nil)
        (save-start-lane msg))))

(defmethod handle-msg "crash" [{{name :name} :data}]
  (when (= (my-name) name)
    (println name " crashed")
    (store :out-of-track true))
  ping)

(defmethod handle-msg "spawn" [{{name :name} :data}]
  (when (= (my-name) name)
    (println name " back on track")
    (store :out-of-track false))
  ping)

(defmethod handle-msg "turboStart" [{{name :name} :data}]
  (when (= (my-name) name)
    (store :turbo-on (fetch :turbo))
    (store :turbo nil))
  ping)

(defmethod handle-msg "turboEnd" [{{name :name} :data}]
  (when (= (my-name) name)
    (do
      (store :turbo-on nil)))
  ping)

(defmethod handle-msg "turboAvailable" [msg]
  (when-not (out-of-track?)
    (store :turbo (:data msg)))
  ping)

(defmethod handle-msg "lapFinished" [{{{:keys [lap millis]} :lapTime {:keys [name]} :car} :data}]
  (when (= (my-name) name)
    (println (format "\nLap %02d %.3f" (inc lap) (double (/ millis 1000)))))
  ping)

(defmethod handle-msg "tournamentEnd" [_]
  (clear!)
  ping)

(defmethod handle-msg :default [_]
  ping)

(defn- init-repl-test-vars []
  (handle-msg (json->clj (slurp "./resources/faketestrace01/000002_gameInit.json"))))
