(ns hwo2014bot.track
  (:use [hwo2014bot.state]
        [hwo2014bot.util]
        [clojure.algo.generic.functor :only [fmap]]))

(defn indexed-pieces []
  (map-indexed vector (pieces)))

(defn pieces-starting-from [index]
  (let [[start next] (split-at index (pieces))]
    (lazy-cat next start)))

(defn turn? [piece]
  (contains? piece :angle))

(defn straight? [piece]
  (not (turn? piece)))

(defn lane-radius [{:keys [angle radius]} {lane-diff :distanceFromCenter}]
  (when (not (nil? radius))
    (let [lane-diff (if (> angle 0) (unchecked-negate-int lane-diff) lane-diff)]
      (+ radius lane-diff))))

(defn steepness
  ([{:keys [shortest-lane-index] :as piece}]
   (steepness piece shortest-lane-index))
  ([{:keys [lane-lengths angle] :as piece} lane-index]
    (if (straight? piece)
      0
      (let [length    (lane-lengths lane-index)]
        (with-precision 8 (/ (Math/abs angle) length))))))

(defn lane-lengths [piece]
  (into {} (for [lane (lanes)]
             { (:index lane)
               (or
                 (:length piece)
                 (let [radius (lane-radius piece lane)]
                   (* (/ (* 2 Math/PI radius) 360) (Math/abs (:angle piece)))))})))

(defn shortest-lane [{:keys [lane-lengths shortest-lane-index]}]
  (if shortest-lane-index
    (find lane-lengths shortest-lane-index)
    (apply min-key val lane-lengths)))

(defn shortest-lane-length [piece]
  (val (shortest-lane piece)))

(defn shortest-lane-index [piece]
  (key (shortest-lane piece)))

(defn- lenghts-for-next-switch
  ([switch-section-sums]
   (lenghts-for-next-switch switch-section-sums []))
  ([switch-section-sums lenghts-per-indexes]
   (if (or (nil? switch-section-sums) (empty? switch-section-sums))
     lenghts-per-indexes
     (let [start-from   (drop-while #(not (contains? % :switch)) switch-section-sums)
           switch-piece (first start-from)
           add-pieces   (take-while #(not (contains? % :switch)) (rest start-from))
           end-switch   (first (filter #(contains? % :switch) (rest start-from)))
           pieces       (filter #(not (nil? %)) (concat [switch-piece] add-pieces [end-switch]))
           lengths      (reduce (fn [sum piece] (merge-with + sum (:lane-lengths piece))) {} pieces)
           lengths      (for [[key val] lengths] {:index key :length val})
           shortest     (apply min-key :length lengths)
           switch-piece (merge switch-piece {:switch-decision-lengths lengths :shortest-lane (:index shortest)})]
       (recur (rest start-from) (conj lenghts-per-indexes switch-piece))))))

(defn- solve-shortest-lanes []
  (let [pieces-with-length  (map-indexed (fn [idx item] (merge item {:index idx :lane-lengths (lane-lengths item)})) (pieces))
        pieces-with-length  (map #(assoc % :shortest-lane-index (shortest-lane-index %)) pieces-with-length)
        switch-split        (split-with #(not (contains? % :switch)) pieces-with-length)
        pieces-from-switch  (concat (second switch-split) (first switch-split))
        pieces-from-switch  (concat pieces-from-switch [(first (second switch-split))])
        switch-sections     (partition-by #(:switch %) pieces-from-switch)
        switch-section-sums (map #(reduce (fn [sum piece] {:lane-lengths (merge-with + (:lane-lengths sum) (:lane-lengths piece))}) %) switch-sections)
        switch-section-sums (map (fn [section] (if (contains? section :switch)
                                                 (update-in section [:lane-lengths]
                                                            #(fmap (fn [len ](/ len 2)) %))
                                                 section)) switch-section-sums)]
    (store :pieces pieces-with-length)
    (store :switch-table (lenghts-for-next-switch switch-section-sums))
    (log-to-file (fetch :switch-table) 2 "b" "switch-table")))

(defn analyze-track []
  (solve-shortest-lanes))
