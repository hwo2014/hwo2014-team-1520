(ns hwo2014bot.car
  (:use hwo2014bot.state
        hwo2014bot.util
        hwo2014bot.track
        clojure.core.matrix.stats
        [clojure.contrib.math :only [expt round]]
        [clojure.set :only [intersection]]))

(defn my-pos [msg]
  (first (filter #(= (get-in % [:id :name]) (my-name)) (msg :data))))

(defn current-piece-index [msg]
  (get-in (my-pos msg) [:piecePosition :pieceIndex]))

(defn current-lap [msg]
  (get-in (my-pos msg) [:piecePosition :lap]))

(defn- current-piece [msg]
  (nth (pieces) (current-piece-index msg)))

(defn- get-relevant-piece [msg]
  (current-piece msg))

(defn- get-next-turn [msg]
  (or (first (filter #(contains? % :angle) (pieces-starting-from (current-piece-index msg))))
      (first (filter #(contains? % :angle) (pieces)))))

(defn- get-turn-direction [piece]
  (let [angle (:angle piece)]
    (cond
      (> angle 0) "Right"
      :else "Left")))

(defn- get-previous-data []
  (first (fetch :race-datas)))

(defn- save-race-data [race-data]
  (let [previous (take 200 (or (fetch :race-datas) '()))]
    (store :race-datas (cons (dissoc race-data :previous-data) previous))))

(defn- calculcate-acceleration [{previous-tick :tick previous-speed :speed} tick speed]
  (when (not (nil? previous-tick))
    (/ (- speed previous-speed) (- tick previous-tick))))

(defn- get-speed [{:keys [piece-distance index piece tick end-lane speed] :as previous} piece-distance-now index-now tick-now]
  (case previous
    nil 0.0
    (let [ticks       (- tick-now tick)
          distance    (if (= index index-now)
                        (- piece-distance-now piece-distance)
                        (+ piece-distance-now (- (get-in piece [:lane-lengths end-lane]) piece-distance)))]
      (case ticks
        0 (if (nil? speed) 0.0 speed)
        (/ distance ticks))
      )))

(defn- calculcate-breaking-distance
  ([speed a] (calculcate-breaking-distance speed a 0))
  ([speed a end-speed]
    (let [t (/ (- end-speed speed) a)
          s (Math/abs (+ (* speed t) (/ (* a (expt t 2)) 2)))]
      (double (inc (round s))))))

(defn- race-data [msg]
  (let [pos                 (my-pos msg)
        piece               (current-piece msg)
        end-lane            (get-in pos [:piecePosition :lane :endLaneIndex])
        start-lane          (get-in pos [:piecePosition :lane :startLaneIndex])
        next-turn           (get-next-turn msg)
        index               (current-piece-index msg)
        piece-distance      (get-in pos [:piecePosition :inPieceDistance])
        lane-lengths-ready  (contains? piece :lane-lengths)
        tick                (:gameTick msg)
        previous-data       (get-previous-data)
        speed               (when tick (get-speed previous-data piece-distance index tick))
        lane-now            (when lane-lengths-ready
                              (cond
                                (= end-lane start-lane) start-lane
                                (> piece-distance (/ (get-in piece [:lane-lengths end-lane]) 2)) end-lane
                                :else start-lane))
        opponents            (filter #(not (= (my-name) (get-in % [:id :name]))) (:data msg))]
    { :msg   msg
      :pos   pos
      :piece piece
      :index index
      :lap   (current-lap msg)
      :angle (:angle pos)
      :start-lane start-lane
      :end-lane end-lane
      :lane-now lane-now
      :lane-lengths-ready lane-lengths-ready
      :piece-distance piece-distance
      :piece-distance-left (- (get-in piece [:lane-lengths end-lane] piece-distance) piece-distance)
      :next-turn next-turn
      :next-turn-direction (get-turn-direction next-turn)
      :tick tick
      :gameTick tick
      :previous-data previous-data
      :speed speed
      :acceleration (when tick (calculcate-acceleration previous-data tick speed))
      :breaking-distance (when tick (when-let [a (acceleration-k)] (calculcate-breaking-distance speed a)))
      :steepness (steepness piece lane-now)
      :turbo (turbo)
      :turbo-on (turbo-on)
      :opponents opponents
      :opponents-ahead (when lane-lengths-ready
                         (let [next-two (take 2 (pieces-starting-from (inc index)))
                               next-idx (map :index next-two)
                               ahead (filter (fn [{{{o-lane :endLaneIndex} :lane o-index :pieceIndex o-dist :inPieceDistance o-lap :lap} :piecePosition}]
                                               (and (= o-lane end-lane)
                                                    (or (and (= o-index index) (> o-dist (+ 1 piece-distance)))
                                                        (some #(= o-index %) next-idx)))) opponents)]
                           (map #(get-in % [:id :name]) ahead)))
      }))


(defn- distance [ticks acc speed]
  (+ (* ticks speed) (* 0.5 acc (expt ticks 2))))

(defn- entry-speed
  ([piece] (entry-speed piece (shortest-lane-index piece)))
  ([piece lane-index]
    (when (turn? piece)
      (let [steepness (steepness piece lane-index)]
        (cond
          (>= steepness 1.5)  2.0
          (>= steepness 1.43) 3.0
          (>= steepness 1.4)  3.5
          (>= steepness 1.2)  5.0
          (>= steepness 1.0)  5.0
          (>= steepness 0.7)  6.0
          (>= steepness 0.6)  6.0
          (>= steepness 0.5)  6.0
          (>= steepness 0.4)  8.0
          (>= steepness 0.3)  8.0
          (>= steepness 0.2)  9.0
          (>= steepness 0.1)  10.0
          :else               100)))))

(defn- throttle-based-on-steepness [{:keys [piece lane-now angle]}]
  (let [steepness (steepness piece lane-now)
        throttle  (cond
                    (>= steepness 2.0)  0.2
                    (>= steepness 1.43) 0.3
                    (>= steepness 1.4)  0.4
                    (>= steepness 1.3)  0.5
                    (>= steepness 1.2)  0.6
                    (>= steepness 1.0)  0.6
                    (>= steepness 0.8)  0.6
                    (>= steepness 0.7)  0.6
                    (>= steepness 0.6)  0.6
                    (>= steepness 0.5)  0.7
                    (>= steepness 0.4)  0.9
                    :else               1.0)
        angle       (Math/abs angle)
        multipl     (* throttle (/ (max (- 45 angle) 0.0) 60))
        multipl     (if (> angle 30) 0 multipl)
        throttle    (+ throttle (* (- 1.0 throttle) multipl))
        throttle    (min 1.0 throttle)]
    throttle))

(defn- should-break? [{:keys [distance entry-speed]} speed safety-buffer]
  (when (> speed entry-speed)
    (let [breaking-distance (calculcate-breaking-distance speed (/ (acceleration-k) 2) entry-speed)
          safe-distance     (- distance (+ safety-buffer breaking-distance))]
      (<= safe-distance 0))))

(defn- update-console-status [{:keys [lap index start-lane end-lane next-turn-direction angle speed throttle acceleration turbo turbo-on] {f-name :name} :function }]
  (when dbgout
    (let [lane (if (= start-lane end-lane) start-lane (str start-lane "->" end-lane))]
      (print (format "\rlap: %02d idx: %02d thr: %.2f s: %6.2f a: %5.2f ang: %6.2f l: %-4s nxt:%5s aK: %4.2f f: %16s trb: %s"
                     lap index throttle speed acceleration angle lane next-turn-direction (acceleration-k) (subs f-name 0 (min 15 (count f-name))) (cond turbo-on "x" turbo "1" :else "0")))
      (flush))))

(defn- get-response [functions race-data]
  (some (fn [f]
          (when-let [res (f race-data)]
            (let [name (last (clojure.string/split (.getName (type f)) #"\$"))
                  res  (if (map? res) [res {}] res)
                  [res info] res
                  race-data (assoc race-data :throttle (case (:msgType res)
                                                         "throttle" (res :data)
                                                         (get-in race-data [:previous-data :throttle]))
                                             :function {:name name
                                                        :info info})]
              (save-race-data race-data)
              (update-console-status race-data)
              (log-to-file (dissoc race-data :previous-data) (get-in race-data [:msg :_counter_]) "b" name)
              res))) functions))

(defn- use-turbo [{:keys [piece lane-lengths-ready speed index] {previous-piece :piece} :previous-data}]
  (when (and lane-lengths-ready (not (turbo-on)))
    (when-let [t (turbo)]
      (when (and (turn? previous-piece) (straight? piece))
        (let [max-turbo-distance (distance (t :turboDurationTicks) (* (t :turboFactor) (acceleration-k)) speed)
              next-safe-pieces (take-while #(< (steepness %) 0.2) (pieces-starting-from index))
              safe-distance (reduce + (map shortest-lane-length next-safe-pieces))]
        (when (> safe-distance max-turbo-distance)
          {:msgType "turbo" :data "pedal to the metal"}))))))

(defn- finish-line [{:keys [lap index]}]
  (when (= (inc lap) (laps))
    (let [remaining (nthrest (pieces) index)
          turn      (some turn? remaining)]
      (when-not turn {:msgType "throttle" :data 1.0}))))

(defn- full-throttle-until-track-analyzed [{:keys [lane-lengths-ready]}]
  (when (not lane-lengths-ready) {:msgType "throttle" :data 1.0}))

(defn- find-acceleration [race-data]
  (when (nil? (acceleration-k))
    (let [samples (take 50 (fetch :race-datas))
          samples (map :acceleration samples)
          samples (filter #(and % (> % 0)) samples)
          samples (take 3 samples)]
      (case (count samples)
        3 (let [average (mean samples)]
            #_(println (format "Acceleration: %.2f variance: %.2f" average (variance samples)))
            (store :acceleration average)
            nil)
        {:msgType "throttle" :data 1.0}))))

(defn- opponent-distance [{my-index :index my-distance :piece-distance my-dist-left :piece-distance-left opponents :opponents} opponent]
  (let [o-data (first (filter #(= (get-in % [:id :name]) opponent) opponents))
        {{o-index :pieceIndex o-dist :inPieceDistance} :piecePosition} o-data]
    (if (= o-index my-index)
      (- o-dist my-distance)
      (let [between (take-while #(not (= (:index %) o-index)) (rest (pieces-starting-from my-index)))
            between (map shortest-lane-length between)]
        (+ my-dist-left (sum between) o-dist)))))

(defn- random-next-lane [current]
  (let [lane-idxs (map :index (lanes))
        lane-idxs (intersection #{(dec current) (inc current)} (set lane-idxs))]
    (rand-nth (vec lane-idxs))))

(defn- toggle-overtake-mode [{:keys [opponents-ahead piece index] {previous-piece :piece} :previous-data :as race-data}]
  (cond
    (empty? opponents-ahead)
      (clear-overtake!)
    (and (overtaking?) (empty? (intersection (set opponents-ahead) (set (overtake-opponents)))))
      (clear-overtake!)
    (overtaking?)
      (do
        (when (overtake-lane)
          (let [on-new-switch (and (:switch piece) (> index (:index previous-piece)))]
            (when on-new-switch
              (store :overtake-lane nil)))))
    (not (overtaking?))
      (do
        (let [history       (take 200 (fetch :race-datas))]
          (let [jammers-ahead (filter (fn [o]
                                (let [o-history  (filter (fn [h] (some #(= % o) (:opponents-ahead h))) history)
                                      o-durance  (count o-history)]
                                  (when (> o-durance 75)
                                    (let [dist-now  (opponent-distance race-data o)
                                          dist-then (opponent-distance (last o-history) o)
                                          gets-away (< (* 2 dist-then) dist-now)]
                                      (not gets-away))))) opponents-ahead)]
            (when (not (empty? jammers-ahead))
              (store :overtake-opponents jammers-ahead)))))))

(defn- get-jammer-switching [{:keys [opponents piece]}]
  (when (overtaking?)
    (first (filter #(not (nil? %)) (for [jammer (overtake-opponents)]
      (let [o-data (first (filter #(= (get-in % [:id :name]) jammer) opponents))
            {{{o-end :endLaneIndex o-start :startLaneIndex} :lane} :piecePosition} o-data
            switching (not (= o-end o-start))]
        (when switching)
          o-end))))))

(defn- switch-lanes-if-needed [{:keys [msg end-lane] :as race-data}]
  (let [jammer-switch-lane (get-jammer-switching race-data)
        overtake-lane (overtake-lane)]
    (toggle-overtake-mode race-data)
    (cond
      (and (not overtake-lane) (= jammer-switch-lane end-lane))
        (let [request-lane (random-next-lane end-lane)]
          (store :overtake-lane request-lane)
          {:msgType "switchLane" :data (if (> request-lane end-lane) "Right" "Left")})
      :else
        (when-let [switch-table   (fetch :switch-table)]
          (let [next-switch    (or
                                 (first (filter #(< (current-piece-index msg) (:index %)) switch-table))
                                 (first switch-table))
                shorter-lane   (:shortest-lane next-switch)
                current-lane   (get-in (my-pos msg) [:piecePosition :lane :endLaneIndex])
                decision-info  (:switch-decision-lengths next-switch)
                shortest-len   (:length (first (filter #(= (:index %) shorter-lane) decision-info)))
                current-len    (:length (first (filter #(= (:index %) current-lane) decision-info)))
                request-lane   (cond
                                 (= shortest-len current-len) current-lane
                                 (> shorter-lane current-lane) (inc current-lane)
                                 (< shorter-lane current-lane) (dec current-lane)
                                 :else current-lane)
                requested-lane (fetch :requested-lane)]
            (when (and
                    (not (= request-lane current-lane))
                    (not (= request-lane requested-lane)))
              (do
                (store :requested-lane request-lane)
                {:msgType "switchLane" :data (if (> request-lane current-lane) "Right" "Left")})))))))

(defn- break-for-turns [{:keys [breaking-distance lane-lengths-ready lane-now speed index piece-distance-left piece] :as race-data}]
  (when (and lane-lengths-ready breaking-distance (acceleration-k))
    (let [safety-buffer       (round (distance 1 (acceleration-k) speed))
          turns-scan-distance (+ safety-buffer breaking-distance)
          turns-scan-distance (max 0 (- turns-scan-distance piece-distance-left))]
      (when (> turns-scan-distance 0)
        (let [next-pieces (rest (pieces-starting-from index))
              next-pieces (rest (reductions (fn [previous piece]
                                              (let [short-lane  (shortest-lane piece)
                                                    short-index (key short-lane)
                                                    short-len   (val short-lane)
                                                    distance-to (get previous :length-sum 0)
                                                    length-sum  (+ distance-to short-len)]
                                                (assoc piece
                                                  :length-sum length-sum
                                                  :distance distance-to
                                                  :shortest-lane short-index
                                                  :entry-speed (entry-speed piece short-index)))) {} next-pieces))
              in          (take-while #(< (:distance %) turns-scan-distance) next-pieces)
              in          (filter turn? in)
              in          (filter #(> (steepness % (:shortest-lane %)) (steepness piece lane-now)) in)
              in          (filter #(should-break? % speed safety-buffer) in)]
          (if-not (empty? in)
            {:msgType "throttle" :data 0.0}))
        ))))

(defn- break-if-angle-too-big [{:keys [angle]}]
  (let [angle (Math/abs angle)]
    (cond
      (> angle 55.0) {:msgType "throttle" :data 0.0}
      (> angle 50.0) {:msgType "throttle" :data 0.2}
      :else nil)))

(defn- adjust-throttle [race-data]
  (let [ throttle (throttle-based-on-steepness race-data)]
    {:msgType "throttle" :data throttle}))

(defn- wait-moment [{:keys [tick]}]
  (when (< tick 200)
    {:msgType "throttle" :data 0.0}))

(defn save-start-lane [msg]
  (let [{:keys [start-lane lap]} (race-data msg)]
    (when (= 0 lap)
      (store :requested-lane start-lane)))
  ping)

(defn throttle-response [msg]
  (with-precision 5
    (let [race-data (race-data msg)
        functions [#_wait-moment
                   full-throttle-until-track-analyzed
                   find-acceleration
                   use-turbo
                   finish-line
                   switch-lanes-if-needed
                   break-for-turns
                   break-if-angle-too-big
                   adjust-throttle
                   ping]]
        (get-response functions race-data))))

