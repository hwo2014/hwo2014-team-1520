(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [clojure.java.io])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message close]]
        [gloss.core :only [string]]
        [hwo2014bot.handlers :only [handle-msg]]
        [hwo2014bot.util]
        [environ.core :only [env]])
  (:gen-class))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "Exception: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "yourCar" (println "Your car")
    "gameInit" (println "Game init")
    "gameStart" (println "Race started")
    "gameEnd" (println "Race ended")
    "tournamentEnd" (println "Tournament ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop
  ([channel] (game-loop channel 0))
  ([channel counter]
    (let [{:keys [gameTick msgType] :as msg} (read-message channel)
          msg (assoc msg :_counter_ counter)]
      (log-msg msg)
      (log-to-file msg counter "a")
      (let [response (handle-msg msg)
            response (assoc response :gameTick gameTick)]
        (when (or (= msgType "gameStart")
                  (and (= msgType "carPositions") gameTick))
          (send-message channel response)
          (log-to-file response counter "b")))
      (when (not (= (msg :msgType) "tournamentEnd"))
        (recur channel (inc counter))))))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))
        track   (env :track)
        cars    (or (env :cars) "1")
        passwd  (env :password)
        msg     (if track
                  { :msgType "joinRace"
                    :data { :botId { :name botname
                                     :key botkey }
                            :trackName track
                            :carCount (read-string cars) }}
                  {:msgType "join" :data {:name botname :key botkey}})
        msg      (if-not passwd msg (assoc-in msg [:data :password] passwd))]
      (println msg)
      (send-message channel msg)
      (game-loop channel)
      (close channel)
      (System/exit 0)))
