(ns hwo2014bot.state
  (:use clojure.contrib.def
        hwo2014bot.util))

(defvar- state (atom {}))

(defn fetch
  ([key] (@state key))
  ([key & keys]
   (get-in @state (conj keys key))))

(defn store [key val]
  (swap! state assoc key val)
  {key val})

(defn clear! []
  (reset! state {}))

(defn pieces []
  (or (fetch :pieces)
      (fetch :race :track :pieces)))

(defn acceleration-k []
  (or (fetch :acceleration)))

(defn lanes []
  (fetch :race :track :lanes))

(defn laps []
  (fetch :race :raceSession :laps))

(defn out-of-track? []
  (fetch :out-of-track))

(defn turbo []
  (fetch :turbo))

(defn turbo-on []
  (fetch :turbo-on))

(defn my-name []
  (fetch :my-car :name))

(defn overtake-opponents []
  (fetch :overtake-opponents))

(defn overtake-lane []
  (fetch :overtake-lane))

(defn overtaking? []
  (not (empty? (overtake-opponents))))

(defn clear-overtake! []
  (when (overtaking?)
    (store :requested-lane nil))
  (store :overtake-lane nil)
  (store :overtake-opponents nil))

