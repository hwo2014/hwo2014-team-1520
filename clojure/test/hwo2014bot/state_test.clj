(ns hwo2014bot.state-test
  (:use midje.sweet
        hwo2014bot.state))

(with-state-changes [(before :facts (clear!))]

  (fact "key value pairs can be stored to state"
    (store :a 1) => (contains {:a 1}))

  (fact "stored key value pairs can be fetched from state"
    (store :a 1)
    (fetch :a) => 1
    (store :b {:c {:d 4}})
    (fetch :b :c :d) => 4))
