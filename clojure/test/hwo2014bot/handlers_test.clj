(ns hwo2014bot.handlers-test
  (:use midje.sweet
        hwo2014bot.handlers
        hwo2014bot.util
        hwo2014bot.state))

(with-redefs [hwo2014bot.util/dbgout true]
  (with-state-changes [(before :facts (clear!))]
    (fact "handler can handle typical race msg sequence"

      (let [dir     (clojure.java.io/file "./resources/faketestrace01")
            files   (filter #(.isFile %) (file-seq dir))
            results (for [file files :let [res (handle-msg (json->clj (slurp file)))]]
                      (do
                        (case (res :msgType)
                          "throttle" (println "")
                          (println res))
                        res))]

        (count results) => (count files)))))