## Clojure bot template for HWO-2014

Install (OS X):

    brew install leiningen

Install (Debian/Ubuntu):

Follow instructions for [Leiningen 2.x](http://leiningen.org/#install).

Adding your own dependencies:

- Uncomment :local-repo from project.clj and run "lein uberjar"
- Commit and push repository folder
- Comment out :local-repo line


